// 1. takes user input in markdown (icons documentation + changelog)
// 2. converts it to html
// 3. add to the userDocs folder, from where it is picked up by other tasks

'use strict';

const { src, dest } = require('gulp');
const markdown = require('gulp-markdown');
const notify = require('gulp-notify');
const plumber = require('gulp-plumber');
const fs = require('fs');
const path = require('path');

module.exports = (packageOptions) => function convertMarkdown(done) {
  const onError = function (error) {
    notify.onError({
      title: 'Styleguide error!',
      message: '<%= error.message %>',
      sound: 'Beep',
    })(error);
    done();
  };


  // get valid paths
  // check for icons/changelog (because those are expected in markdown)
  let validPaths = [];
  packageOptions.docsPaths.filter(function (file) {
    if (file.isExist && (file.key === 'icons' || file.key === 'changelog')) {
      validPaths.push(file.file);
    }
  });
  
  // convert all valid md files, skip others
  if (validPaths.length > 0) {
    return src(validPaths)
      .pipe(plumber({
        errorHandler: onError,
      }))
      .pipe(markdown())
      .pipe(dest(path.resolve(__dirname, '../', 'src/docs/userDocs')));
  }
  else {
    done();
  }
};
