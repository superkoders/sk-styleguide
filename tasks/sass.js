var { src, dest } = require('gulp');
var gulpSass = require('gulp-sass');
var autoprefixer = require('autoprefixer');
var postcss = require('gulp-postcss');
var notify = require('gulp-notify');
var plumber = require('gulp-plumber');
var globImporter = require('node-sass-glob-importer');

module.exports = function sass(done) {
	var onError = function (error) {
		notify.onError({
			title: 'Sass error!',
			message: '<%= error.message %>',
			sound: 'Beep',
		})(error);

		done();
	};

	var settings = {
		importer: [
			globImporter(),
		],
	};

	return src([
		'src/scss/*.scss',
	], {
		cwd: '.',
	})
		.pipe(plumber({
			errorHandler: onError,
		}))
		.pipe(gulpSass(settings))
		.pipe(postcss([
			autoprefixer(),
		]))
		.pipe(dest('./dist/css'));
};
