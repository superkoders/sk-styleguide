// // takes html snippet and wraps it inside basic html markup + add global styles and scripts
// // outputs to styleguide, so it can be used as a live example in documentation

// const { src, dest } = require('gulp');
// const rename = require('gulp-rename');
// const path = require('path');
// const through2 = require('through2');

// module.exports = (packageOptions) => function generateHtmlExamples() {
// 	// return src(path.resolve(__dirname, '../src/docs/componentsDocs/', '**/*.html'))
// 		.pipe(through2.obj(function(file, enc, cb) {
// 			const { name } = path.parse(file.path);
// 			const fileData = JSON.parse(String(file.contents));

// 			src(path.resolve(__dirname, '../src/docs/component-example.njk'))
// 				// .pipe(nunjucksRender({
// 				// 	path: path.resolve(__dirname, '../src/docs/component-example.njk'),
// 				// 	data: {
// 				// 		// name: name,
// 				// 		config: packageOptions
// 				// 	},
// 				// }))
// 				.pipe(
// 					rename({
// 						basename: name,
// 						extname: '.html',
// 					}),
// 				)
// 				.pipe(dest(path.resolve('.', packageOptions.publicPath, packageOptions.styleguideFolder, 'components' )))
// 				.on('error', cb)
// 				.on('end', cb);
// 		}))
// 	};

// Generate live example - take html snippet and wrap it inside barebone html container with CSS and JS

const { src, dest } = require('gulp');
const twigRender = require('gulp-twig');
const rename = require('gulp-rename');
const path = require('path');
const through2 = require('through2');
const nunjucksRender = require('gulp-nunjucks-render');
const notify = require('gulp-notify');
const plumber = require('gulp-plumber');

module.exports = (packageOptions) => function generateHtmlExamples(done) {
  const onError = function (error) {
    notify.onError({
      title: 'Styleguide error! GenerareHtmlExamples is down.',
      message: '<%= error.message %>',
      sound: 'Beep',
    })(error);
    done();
	};

	return src(path.resolve(__dirname, '../', 'src/docs/componentsDocs/', '**/*.html'))
		.pipe(plumber({
			errorHandler: onError,
		}))
		.pipe(through2.obj(function(file, enc, cb) {
			const { name } = path.parse(file.path);
			const templatePath = path.join(file.dirname);
			let componentName = templatePath.split('/'); // name for folder with all snippets of given component
			componentName = componentName[componentName.length - 1];

			return src(path.resolve(__dirname, '../src/docs/component-example.njk'))
				.pipe(plumber({
					errorHandler: onError,
				}))
				.pipe(nunjucksRender({
					path: path.resolve(__dirname + '/../src/docs/'),
					data: {
						name: name,
						componentName: componentName,
						config: packageOptions
					},
				}))
				.pipe(
					rename({
						basename: name,
						extname: '.html',
					}),
				)
				.pipe(dest(path.resolve('.', packageOptions.publicPath, packageOptions.styleguideFolder, 'components/' + componentName + '/examples/' )))
				.on('error', cb)
				.on('end', cb);
		}))
};
