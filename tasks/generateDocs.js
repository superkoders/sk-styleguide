// 1. builds styleguide pages and outputs them to the project's folder

'use strict';

const { src, dest } = require('gulp');
const nunjucksRender = require('gulp-nunjucks-render');
const notify = require('gulp-notify');
const plumber = require('gulp-plumber');
const fs = require('fs');
const path = require('path');

module.exports = (packageOptions) => function generateDocs() {
  const onError = function (error) {
    notify.onError({
      title: 'Styleguide error!',
      message: '<%= error.message %>',
      sound: 'Beep',
    })(error);
  };

  const compileList = packageOptions.docsPaths
    .filter((file) => file.isExist && (file.key != 'codeGuidelines'))
		.map((file) => `${file.key}.njk`);

  return src([
    'about-styleguide.njk',
    'components.njk',
    'templates.njk',
    '!component-detail.njk',
    '!code-guidelines.njk',
    ...compileList
  ], {
    cwd: path.resolve(__dirname + '/../src/docs/'),
  })
    .pipe(plumber({
      errorHandler: onError,
    }))
    .pipe(nunjucksRender({
      path: path.resolve(__dirname + '/../src/docs/'),
      data: {
        relativeDest: './',
        config: packageOptions
      },
    }))
    .pipe(dest(path.resolve('.', packageOptions.publicPath, packageOptions.styleguideFolder)));
};
