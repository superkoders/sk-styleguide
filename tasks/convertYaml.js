// 1. takes input from user (path to yaml file)
// 2. parse it and feeds it to the code-guidelines template
// 3. outputs code guidelines to the styleguide

'use strict';

const { src, dest } = require('gulp');
const notify = require('gulp-notify');
const plumber = require('gulp-plumber');
const nunjucksRender = require('gulp-nunjucks-render');
const path = require('path')
const fs = require('fs');
const YAML = require('yamljs');

module.exports = (packageOptions) => function convertYaml(done) {
  const onError = function (error) {
    notify.onError({
      title: 'Styleguide error!',
      message: '<%= error.message %>',
      sound: 'Beep',
    })(error);
    done();
  };

  // check if user turned CG on, if so, extract data from YAML
  let codeGuidelinesActivated = false;
  let filePath;
  let fileContents;

  packageOptions.docsPaths.filter(function(item) {
    if (item.key === 'codeGuidelines') {
      filePath = item.file;
      fileContents = YAML.parse(fs.readFileSync(filePath, { encoding: 'utf-8' }));
      return codeGuidelinesActivated = item.isExist;
    }
  });
  

  if (codeGuidelinesActivated) {
    return src([
      'code-guidelines.njk',
    ], {
      cwd: path.resolve(__dirname, '../src/docs/'),
    })
      .pipe(plumber({
        errorHandler: onError,
      }))
      .pipe(nunjucksRender({
        path: path.resolve(__dirname + '/../src/docs/'),
        data: {
          config: packageOptions,
          fileContents: fileContents
        },
      }))
      .pipe(dest(path.resolve('.', packageOptions.publicPath, packageOptions.styleguideFolder)));
  }
  else {
    // neni vubec specifikovane ?
    // console.log('Code guidelines path is invalid. If you want generate code guidelines, you need to provide valid path to yaml file.');
    done();
  }
};


