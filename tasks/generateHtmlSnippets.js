// Generate HTML snippets form json and twig for each component
// it is also further used as a foundation for live example

const { src, dest } = require('gulp');
const twigRender = require('gulp-twig');
const rename = require('gulp-rename');
const path = require('path');
const through2 = require('through2');
const notify = require('gulp-notify');
const plumber = require('gulp-plumber');

module.exports = (packageOptions) => function generateHtmlSnippets() {
	const onError = function (error) {
		notify.onError({
				title: 'Styleguide error! GenerareHtmlExamples is down.',
				message: '<%= error.message %>',
				sound: 'Beep',
			})(error);
			done();
		};

	return src(path.resolve(packageOptions.componentsFolder + '/**/*.json'))
	.pipe(plumber({
		errorHandler: onError
	}))
	.pipe(through2.obj(function(file, enc, cb) {
			const { name } = path.parse(file.path);
			let fileData = '';
			const fileContents = String(file.contents);

			// Check if json is valid
			function tryParseJSON (jsonString){
				try {
					fileData = JSON.parse(jsonString);

					// Handle non-exception-throwing cases:
					// Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
					// but... JSON.parse(null) returns null, and typeof null === "object",
					// so we must check for that, too. Thankfully, null is falsey, so this suffices:
					if (fileData && typeof fileData === "object") {
						return fileData;
					} else {
						// console.log("else: invalid JSON!");
					}
				}
				catch (e) {
					fileData = '';
					// console.log("catch: invalid JSON!");
				}
				return false;
			};
			tryParseJSON(fileContents);

			const templatePath = path.join(file.dirname, '../');
			let componentName = templatePath.split('/').slice(0,-1); // name for folder with all snippets of given component
			componentName = componentName[componentName.length - 1];

			src(path.join(templatePath, '*.twig'))
			.pipe(plumber())
			.pipe(
				twigRender({
					data: fileData,
				}),
			)
			.pipe(
				rename({
					basename: name,
					extname: '.html',
				}),
			)
			.pipe(dest(path.resolve(__dirname, '../src/docs/componentsDocs/', componentName)))
			.on('error', cb)
			.on('end', cb);
		}));
};
