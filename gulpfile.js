const { series, parallel } = require('gulp');
const sass = require('./tasks/sass');
const watch = require('./tasks/watch');
const webpack = require('./tasks/webpack');

const build = function build(done) {
	return series(
		parallel(
			webpack,
			sass,
		),
	)(done);
};

const dev = function dev(done) {
	return series(
		build,
		watch,
	)(done);
};

module.exports = {
	sass,
	watch,
	webpack,
	default: dev,
};
