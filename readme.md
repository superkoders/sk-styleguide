# SUPERKODERS styleguide generator
Plugin for generating styleguide from static templates.

![Intro](sg-intro.png)
Generate sidebar with links to all templates for quick and easy access.
---
![Available icons list](sg-icons.png)
Keep track of projects icons with no effort.
---
![Grid view](sg-grid.png)
Leverage tools for quality assurance.

## Installation
Install the package in your command line
```
npm i -D @superkoders/gulp-sk-styleguide
```

## Basic usage
This task takes templates as a source and generates simple styleguide. You can generate it by triggering the task manually, or extend your task for generating templates. That is recommended solution.
```javascript
const gulp = require('gulp');
const skstyleguide = require('gulp-sk-styleguide');

gulp.task('styleguide', function () {
  return gulp.src('./path/to/templates/*.html')
		skstyleguide();
});
```
**Extend task for generating templates**

When templates are generated, take advantage of it and run styleguide immediatelly. Be sure to include `.on('end', function() {...]})`. Templates needs to be ready before we call the styleguide task.
```javascript
const gulp = require('gulp');
const skstyleguide = require('gulp-sk-styleguide');

gulp.task('templates', function () {
	.pipe('...')
	.pipe(dest('...'))
	.on('end', function () {
		skstyleguide();
	});
}
```

## Options
You can adjust behaviour of the styleguide by passiing configuration object. There are several types of settings:

### Important
**Public Path** - path to folder with generated assets

**Templates Folder** - path to the templates directory relative to the public folder

**Styleguide Output** - path where you want output styleguide relative to the public folder

**Relative path** - relative path from template folder to public folder

### Settings
**arrowNav** - navigate between pages with arrow keys. Options: `true` or `false`, default is `true`.

**pinNav** - option to keep nav always open. Options: `true` or `false`, default is `true`.

**gridView** - include grid view support - show grid overlay. Options: `true` or `false`, default is `false`.

**debugView** - include debug view support - highlight empty attributes, deprecated elements etc. Options: `true` or `false`, default is `false`.

**theme** - choose between styleguide theme. Options: `light` or `dark` or `sk` default is `light`.

**iconList** - automatically generate list of available icons. Options: `true` or `false`, default is `false`.

### Additional information
**Project name** - you can specify project name, it will be shown in the styleguide and titles

**logoPath** - specify relative path to the logo image. It will be displayed in sidebar

**iconsFolder** - generate icon list by prociding path for folder with svg icons

### User documentation
**changelog** - specify path to markdown changelog and it will appear as another page in the menu
**codeGuidelines** - specify path to yaml file. The sample will be linked here.
**codeGuidelinesSnippetsFolder** - if you want to include snippets in your guidelines, you can do so. Snippets should be in html files in folder. Specify path to given folder here, link specific snippets inside yaml. <a href="#">How to do that?</a>
**icons** - specify path to markdown documentation of icon system

## Example and default values
```javascript
const gulp = require('gulp');
const skstyleguide = require('gulp-sk-styleguide');

gulp.task('templates', function () {
	.pipe('...')
	.pipe(dest('...'))
	.on('end', function () {
		skstyleguide({
			projectName: 'Project name',
			templatesFolder: 'tpl/',
			styleguideOutput: 'styleguide/',
			iconsFolder: './src/img/bg/icons-svg/',
			publicPath: './web/',
			relativePath: '../',
			logoPath: 'img/logo.svg', // empty by default
			settings: {
				arrowNav: true,
				pinNav: true,
				gridView: true,
				debugView: false,
				iconList: true,
				theme: 'light',
			},
			docs: { // off by default
				changelog: './changelog.md',
				codeGuidelines: './src/docs/code-guidelines.yaml',
				codeGuidelinesSnippetsFolder: './src/docs/code-guidelines-snippets/',
				icons: './src/docs/icons.md',
			},
		});
	});
}
```

## Usage
Open Styleguide in browser and reveal sidebar by moving cursor to left screen edge.


## Author

[![N|Superkoders](http://logo.superkoderi.cz/superkoders.svg)](https://www.superkoders.com)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
