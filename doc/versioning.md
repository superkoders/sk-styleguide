# Versioning

Gulp-sk-styleguide follows [semver](https://semver.org/). When commiting new changes, you can prepend message with keyword. Based on those keywords, we can generate semantic versions and changelog.

## Conventional commit types

### Patch
When you make backwards compatible bug fixes, you want to increase patch version, e.g. `1.0.x`. For that, write commit with fix prefix:

`fix: commit message`.

---
### Minor
When you add features or functionality, still backwards compatible, you want to increase minor version, e.g. `1.x.0`. For that, write commit with feat prefix:

`feat: commit message`.

---
### Major
When you ship new version, which in not backwards compatible, you want to increase major version, e.g. x.0.0. For that, write commit with BREAKING CHANGE prefix:

`BREAKING CHANGE: commit message`.

---
### Others
There are few more variants you can use, for example @commitlint/config-conventional (based on the Angular convention) recommends `chore:`, `docs:`, `style:`, `refactor:`, `perf:`, `test:`, and others. More information on [conventionalcommits.org](https://www.conventionalcommits.org/en/v1.0.0-beta.4/).
