## 1.0.1 (2019-09-26)


### Bug Fixes

* change iframe url without history effect ([faf3080](https://bitbucket.org/superkoders/sk-styleguide/commits/faf3080))
* remove unnecessary comment ([9c1d25b](https://bitbucket.org/superkoders/sk-styleguide/commits/9c1d25b))
* typo ([a896431](https://bitbucket.org/superkoders/sk-styleguide/commits/a896431))


### Features

* add changelog support ([cc8fd89](https://bitbucket.org/superkoders/sk-styleguide/commits/cc8fd89))



# 1.0.0 (2019-09-15)


### Bug Fixes

* remove unnecessary comment ([9c1d25b](https://bitbucket.org/superkoders/sk-styleguide/commits/9c1d25b))



