import * as urlManager from './components/url-manager';
import * as navTools from './components/nav-tools';
import * as navHighlight from './components/nav-highlight';

const components = [
  urlManager,
  navTools,
  navHighlight
];

window.App = {
  run() {
    var target = document;
    components.forEach((component) => component.init(target));
  },

  initComponent(component) {
    return component.init();
  },
};
