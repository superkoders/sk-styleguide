import { on, delegate } from '@superkoders/sk-tools/src/event';
import { parse, stringify } from 'query-string';

var iframe = document.getElementById('my-iframe');
let urlChange = new Event('urlChange', { bubbles: true });

export const dissectUrl = url => {
  const referrer = location.href;
  const urlParts = location.href.split('?');
  let urlParams = parse(urlParts[1] || '');
  // let urlTarget = urlParts[0] + '?' + stringify({ ...urlParams, page: url }, { encode: false });
  let urlTarget = urlParts[0] + '?' + stringify(Object.assign({}, urlParams, { page: url }), { encode: false });
  let urlObj = {
    urlSrc: url, // na co jsem kliknul, kam mirim (pro iframe)
    referrer: referrer, // z jake stranky odchazim
    urlParts: urlParts, // pole, kde je [root, parametry stranky, ze ktere odchazim]
    urlParams: urlParams, // pole vsech parametru stranky, ze ktere odchazim
    pageParameter: urlParams.page, // vypreparovany parametr page
    urlTarget: urlTarget // root + retezec parametru s novym 'page' parametrem; nova kompletni url adresa (pro omnibox)
  };

  return urlObj;
}

export const setUrl = url => { // zmen URL v adresarovem radku, nastav historii
  const urlObj = dissectUrl(url);
  const referrer = urlObj.referrer;
  const urlTarget = urlObj.urlTarget;

  history.pushState({ urlObj, referrer, ajaxify: true }, null, urlTarget);
  changeUrl(history.state);
};

export const changeUrl = ({ urlObj }) => { // aktivuj novou URL
  const urlSrc = urlObj.urlSrc;
  const pageParameter = urlObj.pageParameter;

  if (urlSrc != undefined) {
    iframe.contentWindow.location.replace(urlSrc); // zmen adresu na target
  } else if (pageParameter != undefined) {
    iframe.contentWindow.location.replace(pageParameter); // pri nacteni zadny target neni; pouzijeme adresu v parametru
  } else {
	iframe.contentWindow.location.replace(iframe.src);
    return; // pri nacteni domovske stranky nedelej nic, nech puvodni iframe.src
  }
  document.dispatchEvent(urlChange);
};

// init state
const urlInit = function () {
  const url = location.href; // e.g. http://localhost:3000/styleguide/?page=http://localhost:3000/styleguide/about-styleguide.html
  const urlObj = dissectUrl();
  history.replaceState({ urlObj, referrer: null, ajaxify: true }, null, url);
  changeUrl(history.state);

  // events
  on(window, 'popstate', (event) => {
    if (event.state == null) { return; }
    changeUrl(event.state);
  });

  on(document, 'click', delegate('.js-nav-iframe a', (event, node) => {
    if (node.href.indexOf('/admin/') !== -1) {
      return;
    }
    event.preventDefault();
    setUrl(node.href); // e.g. http://localhost:3000/styleguide/icons.html
  }));
};
urlInit();

