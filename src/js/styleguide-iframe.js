import * as prism from './components/prism';
import * as prismNormalizeWhitespace from './components/prism-normalize-whitespace';
import * as toc from './components/tocbot';

const components = [
  prism,
  // prismNormalizeWhitespace,
  toc
];

window.App = {
  run() {
    var target = document;
    components.forEach((component) => component.init(target));
  },

  initComponent(component) {
    return component.init();
  },
};
